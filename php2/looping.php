<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";

        echo "<h4>LOOPING PERTAMA</h4>";
        for ($a=2; $a <= 20; $a+=2) {
            echo $a . "-  I Love PHP <br>";
        }

        echo "<h4>LOOPING KEDUA<br> </h4>";
        $x = 20;

        do{
            echo "$x - I Love PHP <br>";
            $x-=2;
        } while($x >= 1);


        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
         $nomor = [18, 45, 29, 61, 47, 34];
         echo "Nomor : ";
         print_r($nomor);
         foreach($nomor as $value){
             $item[] = $value%4;
         }
         echo "<br>";
         echo "Sisa bagi 4 Nomor :";
         print_r($item);


        echo "<h3>Soal No 3 Looping Asociative Array </h3>";
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach( $items as $key => $value){
            $itemm= [
                'nama' => $value[0],
                'umur' => $value[1],
                'kota' => $value[2],
            ];
            print_r($itemm);
            echo "<br>";
        }


        echo "<h3>Soal No 4 Asterix </h3>";
        $star=5;
     for($a=$star;$a>0;$a--){
          for($b=$star;$b>=$a;$b--){
               echo "*";
           }
        echo "<br>";
        }
    ?>
</body>
</html>