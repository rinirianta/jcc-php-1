<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh String</h1>
    <?php
    echo "<h3> Kalimat no 1 </h3>";
    $kalimat1 = "Hello PHP";
    $kalimat2 = "I'm ready for the challenges";
    echo "Kalimat 1 = ". $kalimat1 . "<br>";
    echo "Panjang Kalimat 1". strlen($kalimat1)."<br>";
    echo "Jumlah kata =". str_word_count($kalimat1). "<br>";
    echo "<h3>Kalimat no 2 </h3>";
    echo "Kalimat 2 = ". $kalimat2 . "<br>";
    echo "Panjang Kalimat 2". strlen($kalimat2)."<br>";
    echo "Jumlah kata =". str_word_count($kalimat2). "<br>"; 

    echo "<h3>Soal No 2</h3>";
    $kalimat3 = "I love PHP";
    echo "Kalimat 2 =". $kalimat3 . "<br>";
    echo " Kata 1 Kalimat 2 =". substr($kalimat3,0,5). "<br>";
    echo "Kata 2 Kalimat 2 =". substr($kalimat3,6,5). "<br>";

    echo "<h3>Soal No 3 </h3>";
    $kalimat4 = "PHP is old but Good";
    echo "Kalimat 3 =" .$kalimat4 . "<br>";
    echo "Kalimat 3 ubah string =". str_replace("Good", "awesome",$kalimat4);

    
    ?>
</body>
</html>